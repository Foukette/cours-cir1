<button onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Créer évènement</button>
<div id="id01" class="modal">
  <form class="modal-content animate" action="index.php?page=calendar&mois=<?=$month?>&annee=<?=$year?>" method="post">
    <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
    </div>
      <h2>Créer un évènement</h2>
    <div class="container">
      <label for="name">Nom de l'évènement :</label>
      <input id="name" type="text" required name="name">
      <label for="description">Description :</label><br>
      <textarea id="description" required name="description"></textarea>
      <label for="startdate">Date de début : (ex : <?=strftime('%Y-%m-%d %H:%M:%S'); ?>)</label><br>
      <input id="startdate" class="case" type="datetime-local" required name="startdate"  pattern="[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}" value="<?=date('Y-m-d H:i:s'); ?>" ><br>
      <label for="enddate">Date de fin : (ex : <?=date('Y-m-d H:i:s'); ?>)</label><br>
      <input id="enddate" class="case" type="datetime-local" required name="enddate" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}"><br>
      <label for="nb_place">Nombre de places :</label><br>
      <input id="nb_place" class="case" type="number" min="1" required name="nb_place">

      <button type="submit" name='creer' class="connection">Créer l'évènement</button>

    </div>
  </form>
</div>
<script> //javascript pour la beauté
var modal = document.getElementById('id01');

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>

<style>
body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Full-width input fields */
input[type=text], textarea {
    width: 100%;
    display: inline-block;
    padding: 12px;
    margin: 2px 0 10px 0;
    display: inline-block;
    box-sizing: border-box;
    border: 1px solid black;
    border-radius: 10px 10px 10px 10px;
    background: rgb(158,191,216); /*fond case*/
}

/* Set a style for all buttons */
button {
    background: rgba(79,124,200,1);
    color: white;
    padding: 20px 30px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    font-size: 15px;
    border-radius: 10px 10px 10px 10px;
}

h2 {
  text-align: center;
}

.case {
  width: 100%;
  display: inline-block;
  padding: 12px;
  margin: 2px 0 10px 0;
  display: inline-block;
  box-sizing: border-box;
  border: 1px solid black;
  border-radius: 10px 10px 10px 10px;
  background: rgb(158,191,216); /*fond case*/
}
.connection {
  background: rgba(79,124,200,1);
  border: none;
  padding: 15px 30px;
  border: none;
  display: block;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 15px;
  width: 75%;
  color: white;
  cursor: pointer;
  font-size: 20px;
  width: 90%;
  border-radius: 10px 10px 10px 10px;
}
button:hover {
    opacity: 0.8;
}

/* Center the image and position the close button */
.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
    position: relative;
}

.container {
    padding: 16px;
}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 2% auto 2% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 50%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 25px;
    top: 0;
    color: #000;
    font-size: 35px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}

/* Add Zoom Animation */
.animate {
    -webkit-animation: animatezoom 0.6s;
    animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
    from {-webkit-transform: scale(0)}
    to {-webkit-transform: scale(1)}
}

@keyframes animatezoom {
    from {transform: scale(0)}
    to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
}
</style>
