<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="calendar.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Calendrier</title>
    </head>

    <header>
      <?php if ($_SESSION['userInfo']['login'] == 'organizer')
      {
        include('controller/createvent.php');
      } ?>

      <h1>calendrier</h1>

      <article>
        <?php
        echo $_SESSION['userInfo']['login']; ?><br>
        <a class="deconnection" href='index.php?page=deconnection'>Déconnexion</a><br>
      </article>
    </header>

    <body>
      <table>
        <thead>
          <tr>
            <th colspan="7">
              <a href="index.php?page=calendar&mois=<?=$month_previous?>&annee=<?=$year_previous?>"><i class="fa fa-arrow-circle-left" style="font-size:25px"></i></a>
              <?php echo $name_months[$month]; ?>
              <a href="index.php?page=calendar&mois=<?=$month_next?>&annee=<?=$year_next?>"><i class="fa fa-arrow-circle-right" style="font-size:25px"></i></a>
            </th>
          </tr>
          <tr>
            <?php
            for($i = 0; $i < 7; $i++)
            {
              echo '<th>' . $name_days[$i+1] . "</th>";
            }
            ?>
          </tr>
        </thead>

        <tbody>
          <?php
          for ($i=0; $i < $firstday ; $i++)
          {
            ?>
            <td></td>
            <?php
          }
          for($i = $firstday; $i < $nbr_days + $firstday; $i++){
            if ($i%7 == 0)
            {
              echo "<tr>";
            }
            echo '<td>';
            $number = $i-$firstday+1;
            echo $number;
            $count=0;
            $more=0;
            foreach ($donnees as $key)
            {
              $month_s = date("n",strtotime($key['startdate']));
              $year_s = date("Y",strtotime($key['startdate']));
              $day_s = date("d",strtotime($key['startdate']));
              $month_e = date("n",strtotime($key['enddate']));
              $year_e = date("Y",strtotime($key['enddate']));
              $day_e = date("d",strtotime($key['enddate']));

              if ($year_s<=$_GET['annee'] && $year_e>=$_GET['annee'])
              {
                if ($month_s<=$_GET['mois'] && $month_e>=$_GET['mois'])
                {
                  if ($month_s==$_GET['mois'] && $month_e!=$_GET['mois'] && $day_s<=$number)
                  {
                    if ($_SESSION['userInfo']['login'] == 'customer')
                    {
                      $count+=1;
                      if ($count<6)
                      {
                        ?><a href="index.php?page=event&name=<?=$key['name']?>"><?php echo "<br>".$key['name']?></a><?php
                      }
                      elseif($more==0)
                      {
                        $more=1;
                        ?><a href="index.php?page=allevents&day=<?=$number?>"><?php echo "<br> voir plus"?></a><?php
                      }
                    }
                    else if($_SESSION['userInfo']['login'] == 'organizer')
                    {
                      if ($key['organizer_id']==$_SESSION['userInfo']['id'])
                      {
                        $count+=1;
                        if ($count<6)
                        {
                          ?><a href="index.php?page=event&name=<?=$key['name']?>"><?php echo "<br>".$key['name']?></a><?php
                        }
                        elseif($more==0)
                        {
                          $more=1;
                          ?><a href="index.php?page=allevents&day=<?=$number?>"><?php echo "<br> voir plus"?></a><?php
                        }
                      }
                    }
                  }
                  elseif ($month_e==$_GET['mois'] && $month_s!=$_GET['mois'] && $day_e>=$number) {
                    if ($_SESSION['userInfo']['login'] == 'customer')
                    {
                      $count+=1;
                      if ($count<6) {
                        ?><a href="index.php?page=event&name=<?=$key['name']?>"><?php echo "<br>".$key['name']?></a><?php
                      }
                      elseif($more==0) {
                        $more=1;
                        ?><a href="index.php?page=allevents&day=<?=$number?>"><?php echo "<br> voir plus"?></a><?php
                      }
                    }
                    else if($_SESSION['userInfo']['login'] == 'organizer')
                    {
                      if ($key['organizer_id']==$_SESSION['userInfo']['id'])
                      {
                        $count+=1;
                        if ($count<6) {
                          ?><a href="index.php?page=event&name=<?=$key['name']?>"><?php echo "<br>".$key['name']?></a><?php
                        }
                        elseif($more==0) {
                          $more=1;
                          ?><a href="index.php?page=allevents&day=<?=$number?>"><?php echo "<br> voir plus"?></a><?php
                        }
                      }
                    }
                  }
                  elseif ($month_s<$_GET['mois'] && $month_e>$_GET['mois']) {
                    if ($_SESSION['userInfo']['login'] == 'customer')
                    {
                      $count+=1;
                      if ($count<6) {
                        ?><a href="index.php?page=event&name=<?=$key['name']?>"><?php echo "<br>".$key['name']?></a><?php
                      }
                      elseif($more==0) {
                        $more=1;
                        ?><a href="index.php?page=allevents&day=<?=$number?>"><?php echo "<br> voir plus"?></a><?php
                      }
                    }
                    else if($_SESSION['userInfo']['login'] == 'organizer')
                    {
                      if ($key['organizer_id']==$_SESSION['userInfo']['id'])
                      {
                        $count+=1;
                        if ($count<6) {
                          ?><a href="index.php?page=event&name=<?=$key['name']?>"><?php echo "<br>".$key['name']?></a><?php
                        }
                        elseif($more==0) {
                          $more=1;
                          ?><a href="index.php?page=allevents&day=<?=$number?>"><?php echo "<br> voir plus"?></a><?php
                        }
                      }
                    }
                  }
                  elseif ($month_e==$_GET['mois'] && $month_s==$_GET['mois'] && $day_e>=$number && $day_s<=$number) {
                    if ($_SESSION['userInfo']['login'] == 'customer')
                    {
                      $count+=1;
                      if ($count<6) {
                        ?><a href="index.php?page=event&name=<?=$key['name']?>"><?php echo "<br>".$key['name']?></a><?php
                      }
                      elseif($more==0) {
                        $more=1;
                        ?><a href="index.php?page=allevents&day=<?=$number?>"><?php echo "<br> voir plus"?></a><?php
                      }
                    }
                    else if($_SESSION['userInfo']['login'] == 'organizer')
                    {
                      if ($key['organizer_id']==$_SESSION['userInfo']['id'])
                      {
                        $count+=1;
                        if ($count<6) {
                          ?><a href="index.php?page=event&name=<?=$key['name']?>"><?php echo "<br>".$key['name']?></a><?php
                        }
                        elseif($more==0) {
                          $more=1;
                          ?><a href="index.php?page=allevents&day=<?=$number?>"><?php echo "<br> voir plus"?></a><?php
                        }
                      }
                    }
                  }
                }
              }
            }
            echo '</td>';
          }
          ?>
        </tbody>
      </table>
<!-- Insertion du css -->

<style media="screen">
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}
td, th {
  border: 1px solid black;
  text-align: center;
  padding: 8px;
}
a {
  text-decoration: none;
  color: blue;
}
header {
  display:flex;
  justify-content: space-between;
}
h1 {
  font-size: 30px;
}
body {
  background: rgb(192,223,237);
}
</style>


    </body>
</html>
