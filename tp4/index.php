<?php
session_start();
if(isset($_GET['page'])){
  if($_GET['page'] == 'calendar'){
    include('controller/calendar.php');
  }
  else if($_GET['page'] == 'connection'){
    include('controller/connexion.php');
  }
  else if($_GET['page'] == 'deconnection'){
    session_destroy();
    include('controller/connexion.php');
  }
  else if($_GET['page'] == 'event'){
    include('controller/event.php');
  }
  else if($_GET['page'] == 'allevents'){
    include('controller/allevents.php');
  }
  else{
    include('controller/calendar.php');
  }
}
else {
  include('controller/calendar.php');
}
