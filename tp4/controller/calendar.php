<?php
include('model/calendar.php');
if(!isset($_SESSION['userInfo']))
{
  header('Location: index.php?page=connection');
}
if (isset($_GET['mois']))
{
  $month = $_GET['mois'];
}
else
{
  $month = date("n");
}
if (isset($_GET['annee']))
{
  $year = $_GET['annee'];
}
else
{
  $year = date("Y");
}
$nbr_days = date("d", mktime(0, 0, 0, $month+1, 0, $year));
if(isset($_GET['annee']) && isset($_GET['mois']))
{
  $timestamp = strtotime($_GET['annee'].'-'.$_GET['mois']);
}
else
{
  $timestamp = strtotime(date('Y-m'));
}
$firstday = date('w',mktime(0,0,0, date('m',$timestamp),1,date('Y',$timestamp)));
$name_days = array("","Di","Lu","Ma","Me","Je","Ve","Sa");
$name_months = array("","Janvier","Fevrier","Mars","Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","Decembre");

$month_previous = $month - 1;
$month_next = $month + 1;
$year_previous = $year;
$year_next = $year;
if ($month_previous < 1)
{
  $month_previous = 12;
  $year_previous--;
}
if ($month_next > 12)
{
  $month_next = 1;
  $year_next++;
}

$reponse = $bdd->query('SELECT name, startdate, enddate, organizer_id FROM events');
$reponse ->execute();
$donnees = $reponse->fetchall();
include('views/calendar.php');
$reponse->closeCursor();

if (isset($_GET['sup']))
{
  $reponse = $bdd->query('SELECT id FROM events');
  $reponse ->execute();
  $donnees = $reponse->fetchall();
  foreach ($donnees as $key)
  {
    if ($key['id']==$_GET['sup'])
    {
      $id = $key['id'];
      $bdd->query("delete FROM events where id=$id ");
      ?>
      <meta http-equiv="refresh" content="0;URL=index.php?page=calendar&mois=<?=$month?>&annee=<?=$year?>"><?php
    }
  }
}
if (isset($_GET['part']))
{
  $reponse = $bdd->query('SELECT id, nb_place FROM events');
  $reponse ->execute();
  $donnees = $reponse->fetchall();
  foreach ($donnees as $key)
  {
    if ($key['id']==$_GET['part'])
    {
      $place = $key['nb_place']-1;
      $rep=$bdd->prepare('UPDATE events SET nb_place = :place WHERE id = :id');
        $rep->execute(array(
          'place' => $place,
          'id' => $_GET['part']
        ));
        $req = $bdd->prepare('INSERT INTO user_participates_events (id_participant, id_event) VALUES(:id_participant, :id_event)');
        $req->execute(array(
          'id_participant' => $_SESSION['userInfo']['id'],
          'id_event' => $key['id']
        ));
      ?>
      <meta http-equiv="refresh" content="0;URL=index.php?page=calendar&mois=<?=$month?>&annee=<?=$year?>"><?php
    }
  }
}
?>
