<?php
include('model/createvent.php');
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['startdate']) && isset($_POST['enddate']) && isset($_POST['nb_place'])) {
if ($_POST['startdate']<=$_POST['enddate'] && $_POST['startdate']>date('Y-m-d H:i:s')) {
  $req = $bdd->prepare('INSERT INTO events (name, description, startdate, enddate, organizer_id, nb_place) VALUES(:name, :description, :startdate, :enddate, :organizer_id, :nb_place)');
  $req->execute(array(
    'name' => $_POST['name'],
    'description' => $_POST['description'],
    'startdate' => str_replace('T', ' ', $_POST['startdate']),
    'enddate' => str_replace('T', ' ', $_POST['enddate']),
    'organizer_id' => $_SESSION['userInfo']['id'],
    'nb_place' => $_POST['nb_place']
  ));
  ?>
  <meta http-equiv="refresh" content="0;URL=index.php?page=calendar&mois=<?=$month?>&annee=<?=$year?>">
  <?php
}
elseif ($_POST['startdate']>$_POST['enddate']) {
  echo "la date de début doit être inférieure à la date de fin";
}
elseif ($_POST['startdate']<=date('Y-m-d H:i:s')) {
  echo "la date de début doit être supérieure à la date d'aujourd'hui";
}
}
include('views/createvent.php');
?>
