<?php
include('model/connexion.php');
  if(!empty($_POST['login']) && !empty($_POST['password']))
  {
    $reponse = $bdd->prepare('SELECT * FROM users WHERE login = :login ');
    $reponse->execute(array(
      'login' => $_POST['login']
    ));
    $donnees = $reponse->fetch();
    if(password_verify($_POST['password'], $donnees['password'])) {
        $_SESSION['userInfo'] = $donnees;
        header('Location: index.php?page=calendar&mois='.date('n').'&annee='.date('Y').'');
    }
    else {
      echo 'identifiant ou mot de passe invalide';
    }
  }
  include('views/connexion.php');
?>
