<?php
function plateau($size, $board) {
  for ($i = 0; $i < $size; $i++) {
    for ($j = 0; $j < $size; $j++) {
      $board[$i][$j] = rand(0,1);
      if ($board[$i][$j]==0) {
        $board[$i][$j]='O';
      }
      else {
        $board[$i][$j]='..';
      }
    }
  }
  return $board;
}

function tab($size, $board) {
  for ($i = 0; $i < $size; $i++) {
    for ($j = 0; $j < $size; $j++) {
      echo $board[$i][$j];
    }
    echo '<br>';
  }
}

function nei($line, $column, $board) {
  $number = 0;
  if (isset($board[$line+1][$column+1])) {
    if ($board[$line+1][$column+1]=='O') {
      $number++;
    }
  }
  if (isset($board[$line+1][$column])) {
    if ($board[$line+1][$column]=='O') {
      $number++;
    }
  }
  if (isset($board[$line+1][$column-1])) {
    if ($board[$line+1][$column-1]=='O') {
      $number++;
    }
  }
  if (isset($board[$line][$column+1])) {
    if ($board[$line][$column+1]=='O') {
      $number++;
    }
  }
  if (isset($board[$line][$column-1])) {
    if ($board[$line][$column-1]=='O') {
      $number++;
    }
  }
  if (isset($board[$line-1][$column+1])) {
    if ($board[$line-1][$column+1]=='O') {
      $number++;
    }
  }
  if (isset($board[$line-1][$column])) {
    if ($board[$line-1][$column]=='O') {
      $number++;
    }
  }
  if (isset($board[$line-1][$column-1])) {
    if ($board[$line-1][$column-1]=='O') {
      $number++;
    }
  }
  return $number;
}

function gamego($size, $board){
  $boardB = array();
  for($i = 0; $i < $size; $i++){
    $boardB[$i] = array();
  }
  for($i = 0; $i < $size; $i++){
    for($j = 0; $j < $size; $j++){
      if($board[$i][$j] == 'O'){
        if(nei($i, $j, $board) == 2 || nei($i, $j, $board) == 3){
          $boardB[$i][$j] = 'O';
        }
        else{
          $boardB[$i][$j] = ' . ';
        }
      }
      else{
        if(nei($i, $j, $board) == 3){
          $boardB[$i][$j] = 'O';
        }
        else{
          $boardB[$i][$j] = ' . ';
        }
      }
    }
  }
  return $boardB;
  }
