<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>chat</title>
    </head>
    <style>
    form
    {
        text-align:center;
    }
    </style>
    <body>

    <form action="chat.php" method="post">
        <p>
        <label for="nom">Nom</label> : <input type="text" name="nom_auteur" id="nom" /><br />
        <label for="message">Message</label> :  <input type="text" name="message" id="message" /><br />

        <input type="submit" value="Envoyer" />
	</p>
    </form>

<?php
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', '',  [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}

if (!empty($_POST['nom_auteur']) && !empty ($_POST['message'])) {
$reponse = $bdd->prepare('INSERT INTO messages(nom_auteur, message,date) VALUES(:nom_auteur, :message, :date)');
$reponse->execute(array(
    'nom_auteur' => $_POST['nom_auteur'],
    'message' => $_POST['message'],
    'date' => date('Y-m-d h:m:s')
    ));
    $reponse->closeCursor();
}
$reponse = $bdd->query('SELECT nom_auteur, message, date FROM messages ORDER BY id');
while ($donnees = $reponse->fetch())
{
	echo '<p><strong>' . htmlspecialchars($donnees['nom_auteur']) . '</strong> : ' . htmlspecialchars($donnees['message']) ."  (". $donnees['date'] .")".'</p>';
}

$reponse->closeCursor();



?>
    </body>
</html>
